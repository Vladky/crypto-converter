function* currencyGenerator(s) {
	let seed = s
	while (true) {
		seed = Math.abs(parseFloat(Number(seed + Math.random() * 2 - 1).toFixed(2)))
		yield seed
	}
}

export default (seed, title) => {
	const listeners = [],
		interval = null
	const onUpdate = () => listeners.forEach(listener => listener.call())
	return {
		generator: currencyGenerator(seed),
		values: [seed],
		addListener: callback => {
			listeners.push({ call: () => callback() })
		},
		getLast: function() {
			return this.values[this.values.length - 1]
		},
		title: title,
		start: function() {
			this.interval = setInterval(() => {
				this.values.push(this.generator.next().value)
				onUpdate()
			}, 3000)
		},
		stop: function() {
			clearInterval(interval)
		}
	}
}
